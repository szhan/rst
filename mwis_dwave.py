import dimod
import dwave_networkx as dnx
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors as pltcolors


def run_mwis(ket_binarized):
    size = 4
    max_edge_len = 1.5
    node_prob = 0.65
    lagrange = 2.5

    graph_vec = ket_binarized
    print('graph_vec =',graph_vec)

    nodes = []
    for i in range(size**2):
        if graph_vec[i] == 1:
            row = int(np.floor((i)/size))
            col = i % size
            nodes.append([i,row,col])
    print('nodes =',nodes)

    weights = []
    for i in range(len(nodes)):
        for j in range(i+1,len(nodes)):
            dist = np.round(np.sqrt(((nodes[j][1]-nodes[i][1])**2)+((nodes[j][2]-nodes[i][2])**2)),3)
            if dist <= max_edge_len:
                weights.append([nodes[i][0],nodes[j][0],dist])
    print('weights =',weights)

    sampler = dimod.SimulatedAnnealingSampler()

    G = nx.Graph()
    G.add_weighted_edges_from(weights)

    mwis = dnx.maximum_weighted_independent_set(G,lagrange=lagrange,sampler=sampler)
    print('mwis = ',mwis)

    colors = []
    for i in range(len(nodes)):
        if i in mwis:
            colors.append('red')
        else:
            colors.append('blue')
    print('colors =',colors)
    """
    edge_labels = {}
    for i in weights:
        edge_labels[]
    print('edge labels:',edge_labels)
    """

    nx.draw_spring(G, with_labels=True, node_color=colors)
    #print(G.get_edge_data(u=[i for i in range(len(nodes))],v=[j for j in range(len(nodes))]))
    #nx.draw_networkx_edge_labels(G,pos=nx.spring_layout(G),edge_labels=dict(edge_labels))

    data = np.asarray(graph_vec).reshape(size,size)
    # create discrete colormap
    cmap = pltcolors.ListedColormap(['black', 'white'])
    bounds = [0,0.5,1]
    norm = pltcolors.BoundaryNorm(bounds, cmap.N)

    fig, ax = plt.subplots()
    ax.imshow(data, cmap=cmap, norm=norm)

    # draw gridlines
    ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
    ax.set_xticks(np.arange(0.5, size, 1));
    ax.set_yticks(np.arange(0.5, size, 1));

    colored = 0
    empty = 0
    for color in colors:
        if color == 'red':
            colored += 1
        else:
            empty += 1
    if colored == 0 or empty == 0:
        score = 0
    else:
        score = -1*colored/empty
    print('score =',score)

    return score


