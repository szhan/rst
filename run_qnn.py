#!/usr/bin/env python3
import strawberryfields as sf
from strawberryfields.ops import *
import tensorflow as tf


def run_qnn(cutoff=9, depth=2, reps=100, passive_sd=0.1, active_sd=0.001):
	sq_r = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))
	sq_phi = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))
	d_r = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))
	d_phi = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))
	r1 = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))
	r2 = tf.Variable(tf.random_normal(shape=[depth], stddev=passive_sd))
	kappa = tf.Variable(tf.random_normal(shape=[depth], stddev=active_sd))

	def layer(i, q):
		'''
		RSR gates correpond to multiplication by weight matrix,
		D gate to bias addition, and then K gate to activation.
		'''
		Rgate(r1[i]) | q
		Sgate(sq_r[i], sq_phi[i]) | q
		Rgate(r2[i]) | q
		Dgate(d_r[i], d_phi[i]) | q
		Kgate(kappa[i]) | q

	# TODO: handle cases when num_qumodes > 1
	engine, q = sf.Engine(1)
	with engine:
		for k in range(depth):
			layer(k, q[0])

	state = engine.run('tf', cutoff_dim=cutoff, eval=False)
	y_out, var = state.quad_expectation(0)
	ket = state.ket()

	# just for TensorFlow
	target_state = np.zeros([cutoff])
	target_state[1] = 0
	fidelity = tf.abs(tf.reduce_sum(tf.conj(ket) * target_state)) ** 2
	penalty_strength = 1
	cost = (1-fidelity) + penalty_strength*tf.abs(1-state.trace())
	tf.summary.scalar('cost', cost)
	optimiser = tf.train.AdamOptimizer(learning_rate=0.01)
	min_cost = optimiser.minimize(cost)

	session = tf.Session()
	session.run(tf.global_variables_initializer())
	tvars = tf.trainable_variables()
	tvars_vals = session.run(tvars)
	for var, val in zip(tvars, tvars_vals):
		print(var.name, val)
	_, cost_val, fid_val, ket_val = session.run([min_cost, cost, fidelity, ket])
	
	ket_reals = [np.real(ket_val[i] * np.conjugate(ket_val[i])) for i in range(len(ket_val))]
	max_ket_real = np.max(ket_reals)
	ket_reals_norm = ket_reals / max_ket_real
	ket_binarized = [np.heaviside(ket_reals_norm[i] - 0.5, 1) for i in range(len(ket_reals_norm))]

	return(ket_binarized)


if __name__ == "__main__":
	print(run_qnn())


